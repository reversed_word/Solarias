#pragma once
#include <vector>

#include "utils\singleton.h"

namespace engine
{

	class ConfigDatabase : public Singleton<ConfigDatabase> {
	public:
		ConfigDatabase();

		template<typename type_t>
		type_t get_value(const char* field_name_string);

	private:
	};

}