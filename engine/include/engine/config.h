#pragma once

namespace engine
{

	const unsigned int DEFAULT_WINDOW_HEIGHT = 724;
	const unsigned int DEFAULT_WINDOW_WIDTH = 1024;

	extern unsigned int window_height;
	extern unsigned int window_width;

	extern bool borderless;
	extern bool fullscreen;

	extern void CONFIG_INIT();

}