#ifndef SOLARIAS_SOLARIAS_GLOBALS_H_
#define SOLARIAS_SOLARIAS_GLOBALS_H_
#pragma once

#include <thread>

#include <SDL.h>

#include "utils/profiler.h"
#include "utils/thread-pool.h"

#define SOLARIAS_PROJECT_NAME "Solarias"

namespace engine
{

	extern bool g_sdl_initialized; // Solarias/Solarias.cpp

	std::thread* g_graphics_thread;
	SDL_Window* g_window;

	Profiler g_profiler;
	ThreadPool* g_thread_pool;

}

#endif