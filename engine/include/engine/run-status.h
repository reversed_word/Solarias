#ifndef SOLARIAS_SOLARIAS_RUNSTATUS_H_
#define SOLARIAS_SOLARIAS_RUNSTATUS_H_
#pragma once

#include <limits.h>

namespace engine
{

	enum RunStatus {
		ERROR = -1,
		SUCCESS = 0,
		RESTART = 1,
		RUNNING = INT_MAX
	};

}

#endif