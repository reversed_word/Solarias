#ifndef ENGINE_ENGINE_ENGINE_PUBLIC_H_
#define ENGINE_ENGINE_ENGINE_PUBLIC_H_
#pragma once

#include "engine/run-status.h"
#endif

namespace engine
{

	void init_config();

	bool init_graphics();

	void init_logging();

	void init_threadpool();

	void post_config();

	RunStatus run_or_die();

}