#pragma once

#include <vector>

#include "graphics\model.h"
#include "utils\singleton.h"

#define BATCHOBJECT BatchObject::get_singleton()

namespace engine
{

	class BatchObject : public Singleton<BatchObject> {
		std::vector<Model*> models_;
	};

};