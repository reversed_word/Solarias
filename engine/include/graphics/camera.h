#ifndef SOLARIAS_GRAPHICS_CAMERA_H_
#define SOLARIAS_GRAPHICS_CAMERA_H_

#include <graphics\video-mode.h>
#include <utils\multi-dimensional-array.h>

#endif

namespace engine
{

	enum CameraViewMode {
		OTHROGRAPICAL = 0,
		PERSPECTIVE = 1
	};

	struct ViewPort {
		int x_pos_;
		int y_pos_;
		unsigned width_;
		unsigned height_;

		float get_aspect() { return width_ / height_; }
	};

	class Camera {
	public:
		Camera();
		~Camera();

		void set_projection(const float&, const float&, const float&);


		Matrix<float, 4, 4> get_projection() { return float_matrix_; }
		ViewPort get_viewport() { return viewport_; }

	private:
		ViewPort viewport_;

		Matrix<float, 4, 4> float_matrix_;

		float far_viewplane_;
		float near_viewplane_;
		float fov_;
	};

}