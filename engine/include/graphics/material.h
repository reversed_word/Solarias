#ifndef SOLARIAS_GRAPHICS_MATERIAL_H_
#define SOLARIAS_GRAPHICS_MATERIAL_H_
#pragma once

#endif

namespace engine
{

	struct Material {
		float ambient[4], diffuse[4], specular[4], emissive[4];
		float shininess;
		unsigned texture;
		char* texture_file_name;
	};

}