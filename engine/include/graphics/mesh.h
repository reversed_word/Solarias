#ifndef ENGINE_GRAPHICS_MESH_H_
#define ENGINE_GRAPHICS_MESH_H_
#pragma once

#include <vector>

#include "lib\opengl.h"

#include "graphics\vertex-buffer-object.h"
#include "math\vector.h"
#include "utils\multi-dimensional-array.h"

#endif

namespace engine {

	typedef GLuint MaterialIndex;
	typedef GLuint VertexIndex;

	enum ObjectRenderingMode {
		OBJECT_RENDER_MODE_USE_ARRAYS,
		OBJECT_RENDER_MODE_USE_VBO
	};

	struct Vertex {
		char* bone_id;
		Vector3<float> position;
		Vector3<float> normals;
		Vector2<float> texture_coordinates;

		Vertex()
			: bone_id(nullptr)
		{};

		Vertex(char* bone_id_, Vector3<float> position_, Vector3<float> normals_, Vector2<float> texture_coordinates_)
			: bone_id(bone_id_), position(position_), normals(normals_), texture_coordinates(texture_coordinates_)
		{};

		bool operator==(const Vertex& const other) const
		{
			return position == other.position
				&& normals == other.normals
				&& texture_coordinates == other.texture_coordinates
				&& bone_id == other.bone_id;
		}
	};

	class Mesh {
		std::vector<Vertex> vertices_;
		std::vector<VertexIndex> vertex_indices_;

		VertexBufferObject interleaved_buffer_;
		VertexBufferObject index_buffer_;

		ObjectRenderingMode rendering_mode_;

		size_t indices_number_;

		MaterialIndex material_index_;
	public:

		Mesh() : indices_number_(0), rendering_mode_(OBJECT_RENDER_MODE_USE_ARRAYS) {};

		Mesh(std::vector<Vertex> vertices,
			std::vector<VertexIndex> vertex_indices,
			ObjectRenderingMode rendering_mode);

		void reshape(std::vector<Vertex> new_vertices, std::vector<VertexIndex> new_vertex_indices);
		void update_shape(std::vector<Vertex> new_vertices, std::vector<VertexIndex> new_vertex_indices);

		void create(bool use_buffer = true);
		void update();
		void clear();

		void render();
		void render(ObjectRenderingMode);

		MaterialIndex get_material();
		void set_material(MaterialIndex);

		ObjectRenderingMode get_render_mode();
		void set_render_mode(ObjectRenderingMode);

		bool is_buffered();

	private:

		void update_buffers();

		void render_from_buffers();
		void render_from_arrays();
	};

	/**
	* ADL overloading for Vertex
	*/
	/*bool operator==(const Vertex& const first, const Vertex& const second)
	{
		return first.position == second.position
			&& first.normals == second.normals
			&& first.texture_coordinates == second.texture_coordinates
			&& first.bone_id == second.bone_id;
	}*/

}; // END namespace engine