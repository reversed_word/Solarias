#ifndef ENGINE_GRAPHICS_MODEL_H_
#define ENGINE_GRAPHICS_MODEL_H_
#pragma once

#include <memory>

#include "graphics\mesh.h"

#endif

namespace engine
{

	enum ModelFileFormat {
		WAVEFORMOBJ,
		MILKSHAPE3D
	};

	struct ModelFile {
		ModelFileFormat file_format;
		const char* file_path;
	};

	struct MeshIndexSet {
		VertexIndex position_index;
		unsigned texture_coordinates_index;
		unsigned normals_index;

		MeshIndexSet()
			: position_index(0), texture_coordinates_index(0), normals_index(0)
		{
		};

		Vertex to_vertex(char* const bone_name,
			std::vector<Vector3<float>>* const position_list,
			std::vector<Vector2<float>>* const texture_coordinate_list,
			std::vector<Vector3<float>>* const normals_list) const
		{
			return Vertex(bone_name,
				position_list->at(position_index),
				normals_list->at(normals_index),
				texture_coordinate_list->at(texture_coordinates_index));
		}

		size_t operator()(const MeshIndexSet& const set) const
		{
			return static_cast<size_t>(set.position_index 
				^ set.texture_coordinates_index 
				^ set.normals_index);
		}
		bool operator==(const MeshIndexSet& const other) const
		{
			return position_index == other.position_index
				&& texture_coordinates_index == other.texture_coordinates_index
				&& normals_index == other.normals_index;
		}
		bool operator!=(const MeshIndexSet& const other) const
		{
			return position_index != other.position_index
				&& texture_coordinates_index != other.texture_coordinates_index
				&& normals_index != other.normals_index;

		}
	};

	/* bool operator==(const MeshIndexSet& const first, const MeshIndexSet& const second)
	{
		return first.position_index == second.position_index
			&& first.texture_coordinates_index == second.texture_coordinates_index
			&& first.normals_index == second.normals_index;
	} */

	class Model {

		struct ModelInfo {
			std::vector<std::shared_ptr<Mesh>> meshes;
		};

	public:
		std::shared_ptr<ModelInfo> shared_model_info_;

		/**
		* Sends the model's data to the VBO for rendering
		*/
		void render();
		
		/**
		* Loads Model from path to file
		* @param model_file_path Path to model file
		* @return false if it has errors, otherwise true
		*/
		bool load_from(const char* model_file_path);
		/**
		* Loads Model from ModelFile data
		* @param model_file Input data for the loader to parse
		* @return false if it has errors, otherwise true
		*/
		bool load_from(const ModelFile& model_file);

		/**
		* Loads Model from WAVEFORM OBJ (.obj) files
		* @param model_file_path Path to model file to be loaded
		* @return false if it has errors, otherwise true
		*/
		bool load_from_obj(const char* model_file_path);

	};
}; // END namespace engine