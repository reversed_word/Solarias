#ifndef ENGINE_GRAPHICS_RENDERER_H_
#define ENGINE_GRAPHICS_RENDERER_H_
#pragma once

#include "utils/singleton.h"

#endif

#define RENDERER Renderer::get_singleton()

#define FRAMEBUFFER 0

namespace engine
{

	//template<typename T, size_t rows, size_t columns>
	//using Matrix = T[rows][columns];

	class Renderer : public Singleton<Renderer> {
		typedef unsigned FrameBuffer;
		typedef unsigned FrameTexture;

	public:
		Renderer();
		~Renderer();

		void draw_frame();
		void finalize_frame();

	protected:

	private:
		void prepare_frame_buffer();
		void setup_opengl();

		void begin_frame();
		void finish_frame();

		FrameBuffer framebuffer_;
		FrameTexture frame_texture_;
	};

}; // END namespace engine