#ifndef ENGINE_GRAPHICS_VERTEX_BUFFER_OBJECT_H_
#define ENGINE_GRAPHICS_VERTEX_BUFFER_OBJECT_H_
#pragma once

#include "lib\opengl.h"

#endif

namespace engine {

	/*enum VertexBufferObjectUsageFlag {
		VBO_USAGE_FLAG_ARRAY_BUFFER,
		VBO_USAGE_FLAG_ELEMENT_ARRAY_BUFFER
	};*/

	class VertexBufferObject {

		unsigned buffer_id_;

		unsigned usage_flag_;

		static unsigned current_array_buffer_id_;
		static unsigned current_element_array_buffer_id_;

	public:

		VertexBufferObject() : buffer_id_(0)
		{

		}
		~VertexBufferObject()
		{
			if (buffer_id_ != 0) // don't delete the defualt fallback buffer
				glDeleteBuffers(1, &buffer_id_);
		}

		void create();
		void destroy();

		void bind(const unsigned flag);
		void force_bind(const unsigned flag);
		void unbind();

		static void unbind(const unsigned flag);
		static void unbind_all();

		static unsigned get_current_array_buffer() {
			return current_array_buffer_id_;
		}
		static unsigned get_current_element_array_buffer() {
			return current_element_array_buffer_id_;
		}

		bool created() const {
			return buffer_id_ != 0;
		}
		unsigned get_id() const {
			return buffer_id_;
		}
		unsigned get_attribute() const {
			return usage_flag_;
		}

	};

}; // namespace engine