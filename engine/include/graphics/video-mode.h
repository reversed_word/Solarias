#ifndef SOLARIAS_SGUI_WINDOW_H_
#define SOLARIAS_SGUI_WINDOW_H_
#pragma once

#include <SDL.h>

#include "utils/singleton.h"
#endif

/**
Currently unclear if I should use an enum or a struct/union of bools.

enum DisplayMode
{
	WINDOWED,
	BORDERLESS,
	FULLSCREEN
};
*/

#define VIDEOMODE VideoMode::get_singleton()

namespace engine
{

	class VideoMode : public Singleton<VideoMode> {
	public:
		VideoMode();
		~VideoMode();

		bool init_sdl();

		bool update_resolution(int width, int height_);

		void update_position(int x_pos, int y_pos);

		void set_window_caption(const char* new_caption_string);
		const char* get_window_caption() const;

		void set_fullscreen_enabled(bool state_bool);
		bool is_fullscreen_enabled() const;
		bool toggle_fullscreen();

		bool is_window_open() const;
		bool open_window();
		bool shut_window();
		void shutdown();

		unsigned int get_width() const;
		unsigned int get_height() const;
		float get_ratio() const;

		unsigned int get_bpp() const;
		unsigned int get_best_bpp() const;

		SDL_Window* get_window() const;
	private:
		bool set_video_mode_or_die();

		unsigned int width_;
		unsigned int height_;

		bool fullscreen_bool_;
		bool is_open_bool_;

		SDL_Window* sdl_window_;
		SDL_GLContext gl_context_;
	};

}