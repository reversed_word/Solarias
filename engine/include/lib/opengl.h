#ifndef ENGINE_LIB_OPENGL_H_
#define ENGINE_LIB_OPENGL_H_
#include "lowlevel/external_libraries/opengl_.h"

#define USE_GLEW

#if defined(USE_GLEW)
#include "GL\glew.h"

#if defined(_WIN32)
#include "GL\wglew.h"
#endif
#else
#include <gl\GL.h>
#include <gl\GLU.h>
#endif

// Vertex attributes for OpenGL vertex attribute functions (indices)
#define VERTEX_ATTRIBUTE_POSITION		(GLuint) 0
#define VERTEX_ATTRIBUTE_NORMALS		(GLuint) 1
#define	VERTEX_ATTRIBUTE_TEXCOORDINATES (GLuint) 2

// Index for the buffer specialized for VAO (Vertex Array Object) pattern
#define VERTEX_ARRAY_OBJECT             (GLuint) 0

namespace engine
{
	void opengl_setup_buffers();
}

#endif
