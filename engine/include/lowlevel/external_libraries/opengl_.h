/**
* This header is dedicated for OpenGL, the underscore in its name is for easier identification
* between this and the actually used one
*/


#ifndef ENGINE_LOWLEVEL_EXTERNAL_LIBRARIES_OPENGL__H_
#define ENGINE_LOWLEVEL_EXTERNAL_LIBRARIES_OPENGL__H_

#if OS_WIN 
// wgl.h is a private header and should only be included from here.
// if this isn't defined, it'll complain.
#define WGL_HEADER_NEEDED
#include "lowlevel/system_dependent/win/wgl.h"
#endif

#endif
