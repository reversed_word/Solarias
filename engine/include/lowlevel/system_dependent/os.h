/** 
* Header file for Operative System identification for the compiler and preprocessor marcos
*/

// Windows
#if defined(_WIN32)
#define OS_WIN 1
#else
#define OS_WIN 0
#endif