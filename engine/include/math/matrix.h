#ifndef SOLARIAS_MATH_MATRIX_H_
#define SOLARIAS_MATH_MATRIX_H_

#include <array>
#include <vector>
#include <iostream>

#include "shared\meta.h"

#endif

namespace engine
{

	template <typename T, size_t ... dimensions>
	class Matrix {
		typedef T result_type;
		typedef std::initializer_list<result_type> type_init_list;

		static_assert(sizeof ... (dimensions) > 0, "At least one dimension needs to be defined, must be higher than 0!");
		// static_assert(dimensions > 1, "Dimension value must be a real integer at least 1!");

	public:
		static constexpr size_t size()
		{
			return meta::product(dimensions ...);
		}

		static constexpr size_t dimension_amount()
		{
			return meta::division(dimensions ...);
		}

		Matrix()
		{

		}

		Matrix(std::initializer_list<type_init_list> init_list) : dimensions_i_({ dimensions... })
		{
			/*
			std::vector<std::vector<result_type>> tmp_vector(init_list.size(), std::vector<result_type>(init_list.begin()->size()));
			const std::initializer_list<result_type>* tmp_list = init_list.begin();

			for (auto iterator = tmp_vector.begin(); iterator != tmp_vector.end(); ++iterator)
			{
				iterator->insert(iterator->end(),tmp_list->begin(),tmp_list->end());
				++tmp_list;
			}
			*/

			initialize(init_list);

			/*

			dimensions_i_ = dimensions;

			std::cout << dimensions[0] << dimensions[1] << std::endl;

			size_t row_size_t = init_list.size();
			size_t column_size_t = init_list.begin()->size();

			std::cout << row_size_t << std::endl;
			std::cout << column_size_t << std::endl;

			//result_type** arrt = init_list;

			matrix_data_array_ = (result_type**)malloc(sizeof(result_type*) * row_size_t);
			for (int iterator = 0;
			iterator < sizeof(matrix_data_array_) / sizeof(matrix_data_array_[0]);
				++iterator)
			{
				matrix_data_array_[iterator] = (result_type*)malloc(sizeof(result_type) * column_size_t);
				matrix_data_array_[iterator] = new result_type[column_size_t];
			}

			//TODO: FINISH THIS
			for (int row_iterator = 0; row_iterator < row_size_t; row_iterator++)
			{
				auto init_row_ptr = init_list.begin();
				for (int column_iterator = 0; column_iterator < column_size_t; column_iterator++)
				{
					auto init_column_ptr = init_row_ptr->begin();
					if (init_column_ptr != nullptr || column_iterator < column_size_t)
						matrix_data_array_[row_iterator][column_iterator] = (result_type&)init_column_ptr;
					else
						matrix_data_array_[row_iterator][column_iterator] = 0;
					++init_column_ptr;
				}
				++init_row_ptr;
			}

				std::cout << sizeof(int**[10]) << " " << sizeof(int*) << std::endl;
				std::cout << sizeof(int**) << " " << sizeof(int*) << std::endl;
				std::cout << sizeof(matrix_data_array_) << " " << sizeof(matrix_data_array_[0]) << std::endl;
				std::cout << sizeof(matrix_data_array_) / sizeof(matrix_data_array_[0]) << std::endl;

			int arr_row_i = 0;
			int arr_column_i = 0;

			*/

		}
		~Matrix()
		{
			/*
			for (auto iterator = 0; iterator < sizeof(martix_type_) / sizeof(martix_type_[0]); iterator++)
				delete[] martix_type_[iterator];
			delete[] martix_type_;
			*/
		}

		void print() const
		{
			for (auto iterator = 0; iterator < get_row_amount(); iterator++)
			{
				std::cout << "| ";
				for (auto inner_iterator = 0; inner_iterator < get_column_amount(); inner_iterator++)
					std::cout << matrix_data_array_[iterator][inner_iterator];
				std::cout << " |" << std::endl;
			}
		}

		unsigned length() const
		{
			return get_row_amount();
		}

		unsigned get_row_amount() const
		{
			return sizeof(matrix_data_array_) / sizeof(matrix_data_array_[0]);
		}

		unsigned get_column_amount() const
		{
			return sizeof(matrix_data_array_[0]) / sizeof(matrix_data_array_[0][0]);
		}
		/*
		Matrix<result_type>& operator=(const result_type martix[][])
		{
			martix_type_ = martix;

			return *this;
		}

		Matrix<result_type>& operator=(const Matrix& matrix)
		{
			martix_type_ = matrix.martix_type_;

			return *this;
		}
		*/

		std::vector<result_type> operator[](unsigned row)
		{
			return martix_data_array_[row];
		}

		/*
	#define IN_ROW pos[0]
	#define IN_COLUMN pos[1]

		result_type operator[](unsigned pos[])
		{
			if (martix_vector_ == nullptr)
				return 0;
			if (IN_ROW < get_row_amount())
			{
				if (IN_COLUMN < get_column_amount())
					return martix_vector_[IN_ROW][IN_COLUMN];
				printf("column out of bounds!");
			}
			printf("row out of bounds!");
			return 0;
		}

	#undef IN_ROW
	#undef IN_COLUMN

		*/

	private:

		void initialize(std::initializer_list<type_init_list> init_list)
		{
			size_t init_row_size = init_list.size();
			size_t init_column_size = init_list.begin()->size();

			matrix_data_vector_.reserve(size());

			for (int row_iterator = 0; row_iterator < init_row_size; row_iterator++)
			{
				auto init_list_row_ptr = init_list.begin();

				for (int column_iterator = 0; column_iterator < init_column_size; column_iterator++)
				{
					auto init_list_column_ptr = init_list_row_ptr->begin();

					matrix_data_vector_.push_back(init_list_column_ptr);

					++init_list_column_ptr;
				}

				++init_list_row_ptr;
			}
		}

		size_t dimensions_i_[COUNT(dimensions)];

		result_type** matrix_data_array_;

		std::vector<result_type> matrix_data_vector_;
	};

}

/*
#define RESTRICT(type) template <>	class Matrix < type >	{ typedef type	result_type;};

RESTRICT(bool)

RESTRICT(char)
RESTRICT(short)
RESTRICT(int)
RESTRICT(long)
RESTRICT(long long)
RESTRICT(float)
RESTRICT(double)
RESTRICT(long double)

RESTRICT(unsigned char)
RESTRICT(unsigned short)
RESTRICT(unsigned)
RESTRICT(unsigned long)
RESTRICT(unsigned long long)

#undef RESTRICT
*/
