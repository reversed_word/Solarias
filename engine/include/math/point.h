#ifndef SOLARIAS_MATH_POINT_H_
#define SOLARIAS_MATH_POINT_H_

#endif

namespace engine
{

	struct Point {
		Point();
		Point(int x_i, int y_i);

		void set_x(int new_i);
		void set_y(int new_i);

		int get_x() { return x_position_i_; }
		int get_y() { return y_position_i_; }
	private:
		int x_position_i_;
		int y_position_i_;
	};

}