#ifndef SOLARIAS_MATH_RECTANGLE_H_
#define SOLARIAS_MATH_RECTANGLE_H_
#pragma once

#endif

namespace engine
{

	struct Rectangle {
		Rectangle();
		Rectangle(int width_i, int height_i);

		virtual ~Rectangle();

		void set_width(int new_width_i);
		void set_height(int new_height_i);

		int get_width() { return width_; };
		int get_height() { return height_; };

	private:
		int width_;
		int height_;
	};

}