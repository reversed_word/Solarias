#ifndef ENGINE_MATH_VECTOR_H_
#define ENGINE_MATH_VECTOR_H_
#pragma once

#endif

namespace engine
{

	template <typename T>
	struct Vector2 {
		T x, y;

		Vector2() {};
		Vector2(T X, T Y) : x(X), y(Y) {};

		bool operator==(const Vector2& const other) const
		{
			return x == other.x
				&& y == other.y;
		}
		bool operator!=(const Vector2& const other) const
		{
			return x != other.x
				&& y != other.y;
		}
	};

	template <typename T>
	struct Vector3 {
		T x, y, z;

		Vector3() {};
		Vector3(T X, T Y, T Z) : x(X), y(Y), z(Z) {};

		bool operator==(const Vector3& const other) const
		{
			return x == other.x
				&& y == other.y
				&& z == other.z;
		}
		bool operator!=(const Vector3& const other) const
		{
			return x != other.x
				&& y != other.y
				&& z != other.z;
		}
	};

}