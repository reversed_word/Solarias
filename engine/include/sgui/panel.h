#ifndef SOLARIAS_SGUI_PANEL_H_
#define SOLARIAS_SGUI_PANEL_H_
#pragma once

#include <vector>

#include "math/point.h"
#include "math/rectangle.h"

#include "scheme.h"
#include "sgui-marcos.h"

#endif

namespace engine
{

	class Panel {
		DECLARE_OBJECT_CLASS_NO_BASE(Panel);

	public:
		Panel();
		Panel(Panel* parent_panel);
		Panel(Panel* parent_panel, const char* name_string);
		Panel(Panel* parent_panel, const char* name_string, Scheme scheme);

		/**
		* VIRTUAL MEMBERS
		*/

		virtual ~Panel();

		virtual void set_visiblity(bool state_b);
		virtual bool is_visible();

		virtual void set_enabled(bool state_b);
		virtual bool is_enabled();

		virtual void apply_scheme_settings();

		virtual void set_mouse_input_enabled(bool state_b);
		virtual void set_keyboard_input_enabled(bool state_b);
		virtual bool is_mouse_input_enabled();
		virtual bool is_keyboard_input_enabled();

		virtual void set_parent(Panel* parent_panel);
		virtual Panel* get_parent();

		virtual bool should_draw();
		virtual void draw();

		/**
		* REAL MEMBERS
		*/

		bool is_root() const;

		int children_count();
		std::vector<Panel*> get_children_panels();
		void add_child(Panel* child_panel);

		int find_child_index_by_name(const char* name_string);
		Panel* find_child_by_name(const char* name_string);

		void set_name(const char* new_name_string);
		const char* get_name() const { return name_string_; };
		const char* get_class_name_string() const;

		void set_position(int x_i, int y_i);
		void set_position(Point new_position_point);
		Point get_position();

		void set_x_position(int x_i);
		void set_y_position(int y_i);
		int get_x_position();
		int get_y_position();

		bool is_within();
		bool is_within(Panel* target_panel);

		Point local_to_screen();
		Point screen_to_local();

		void set_size(int width_i, int height_i);
		void set_size(Rectangle size_rectangle);
		Rectangle get_size();

		void set_autosize_enabled(bool state_b);
		bool is_autosize_enabled();

		void set_width(int new_width_i);
		void set_height(int new_height_i);
		int get_width();
		int get_height();

		void set_minimum_size(int with_i, int height_i);
		void set_minimum_size(Rectangle size_rectangle);
		Rectangle get_minimum_size();

		void set_minimum_width(int new_width_i);
		void set_minimum_height(int new_height_i);
		int get_minimum_width();
		int get_minimum_height();

		void set_z_index(int z_index_i);
		int get_z_index();

		void set_opacity(float opacity_x);
		float get_opacity();

	private:
		const char* name_string_;

		bool is_root_b_;

		int position_x_i_;
		int position_y_i_;
		int z_index_i_;

		int width_;
		int height_;

		float opacity_x_;

		std::vector<Panel*> children_vector_;
	};

}