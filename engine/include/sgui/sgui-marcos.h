#ifndef SOLARIAS_SGUI_SGUI_MARCOS_H_
#define SOLARIAS_SGUI_SGUI_MARCOS_H_
#pragma once

#define DECLARE_OBJECT_CLASS_NO_BASE( classname )						\
public:																	\
	static const char* get_object_class_name() { return #classname; }	\
	static const char* get_object_class_base_name() { return nullptr; }	\
private:																\
	typedef classname this_class_										\

#endif
