#ifndef SOLARIAS_SHARED_META_H_
#define SOLARIAS_SHARED_META_H_

#include <array>
#include <initializer_list>

namespace meta {

	enum ArithmeticOperator
	{
		ADDITION,
		SUBSTRACTION,
		MULTIPLICATION,
		DIVISION
	};

	template<typename ... T_values>
	struct expr_result
	{

	};

#define COUNT(x) sizeof...(x)

	template<typename ... T>
	constexpr static const unsigned count(T ... values)
	{
		return sizeof...(values);
	}

	template<typename T_One, typename T_Two>
	constexpr static T_One division(T_One value_one, T_Two value_two)
	{
		return value_one / value_two;
	}

	template<typename T_One, typename T_Two, typename ... T_Other>
	constexpr static T_One division(T_One value_one, T_Two value_two, T_Other ... other)
	{
		return division(value_one / value_two, other...);
	}

	template<typename T_One, typename T_Two>
	constexpr static T_One product(T_One value_one, T_Two value_two)
	{
		return value_one * value_two;
	}

	template<typename T_One, typename T_Two, typename ... T_Other>
	constexpr static T_One product(T_One value_one, T_Two value_two, T_Other ... other)
	{
        return product(value_one * value_two, other...);
	}

	/*
	template<typename T>
	constexpr static const variadic_to_array(T ... values)
	{
		T return_array[COUNT(values)];
		std::initializer_list<T> li({ values... });
		std::copy(li.begin(), li.end(), return_array[0]);
		
		return return_array;
	}
	*/
};

#endif
