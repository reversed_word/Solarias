#ifndef SOLARIAS_UTILS_CHANNELTYPE_H_
#define SOLARIAS_UTILS_CHANNELTYPE_H_

namespace engine
{

	enum ChannelType {
		UNKNOWN_LOG = 0,
		NORMAL_LOG = 1,
		WARNING_LOG = 2,
		ERROR_LOG = 3,
		INFO_LOG = 4,
		DEBUG_LOG = 5
	};

}

#endif