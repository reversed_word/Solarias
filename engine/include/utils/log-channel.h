#ifndef SOLARIAS_UTILS_LOGCHANNEL_H_
#define SOLARIAS_UTILS_LOGCHANNEL_H_
#pragma once
#include <deque>

#include "utils/channel-type.h"
#endif

namespace engine
{

	class LogChannel {
	public:
		friend class Logger;

		LogChannel(const char*);
		LogChannel(const char*, ChannelType);
		~LogChannel();

		ChannelType channel_type_;

		std::string get_chan_name_string() const
		{
			return chan_name_str_;
		}

		std::string get_log_file_path_string() const
		{
			return std::string("log/" + chan_name_str_ + ".log");
		}

		void close();
		void ensure();
		bool file_exists();
		FILE* open();
		void write(std::string const message);
		void write(const char* const format, ...);

	private:
		std::string chan_name_str_;

		std::fstream ensure_file();

		// std::deque<char*> strings_;
	};

}
