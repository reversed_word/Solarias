#ifndef SOLARIAS_UTILS_LOGGER_H_
#define SOLARIAS_UTILS_LOGGER_H_
#pragma once

#include <exception>
#include <iostream>
#include <list>
#include <map>

#include "utils/singleton.h"
#include "utils/log-channel.h"
#endif

#define LOGGER Logger::get_singleton()
#define LOGERROR(message) Logger::get_singleton()["error"]->write(message)

namespace engine
{

	/**
	* Class for all my logging needs
	*
	* Singleton class
	*/
	class Logger : public Singleton<Logger> {
	public:
		friend class LogChannel;

		Logger();
		~Logger();

		void setup();

		void add_channel(const char* channel_name_str);
		void add_channel(const char* channel_name_str, ChannelType channel_type);
		void all_write(const char* message_text);
		bool channel_exists(const char*);
		std::list<const char*> get_channel_names();
		std::list<const char*> get_channel_names_by_type(ChannelType channel_type);
		// static void channel_write(const char* channame_str, const char* message_text);

		LogChannel* operator[](const char* channel_name_str) const;
	protected:

	private:
		// Whether the object expires, it will save to backup_map_,
		// when re-instantized, the constructor will check if it is
		// not the first time it was instantized
		bool backup_is_not_empty();

		std::map<const char*, LogChannel*> log_channel_map_;

		static std::map<const char*, LogChannel*> backup_map_;
	};

}