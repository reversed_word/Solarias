#ifndef SOLARIAS_UTILS_MULTI_DIMENSIONAL_ARRAY_H_
#define SOLARIAS_UTILS_MULTI_DIMENSIONAL_ARRAY_H_
#pragma once

#include <array>

#endif

namespace engine
{

	template<typename T, size_t first_dimension, size_t ... other_dimensions>
	struct MultiDimensionalArray {
		typedef typename MultiDimensionalArray<T, other_dimensions ...>::type nested;
		typedef std::array<nested, first_dimension> type;
	};

	template<typename T, size_t first_dimension>
	struct MultiDimensionalArray<T, first_dimension> {
		typedef std::array<T, first_dimension> type;
	};

	template<typename T, size_t row_size, size_t column_size>
	using Matrix = typename MultiDimensionalArray<T, row_size, column_size>::type;
	//typedef MultiDimensionalArray<T, row_size, column_size> Matrix;

}