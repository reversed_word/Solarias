#ifndef SOLARIAS_UTILS_SINGLETON_H_
#define SOLARIAS_UTILS_SINGLETON_H_
#pragma once

#include <cassert>

namespace engine
{

	template<typename T> class Singleton {
	public:
		static T& get_singleton()
		{
			if (!is_initialized())
				instantizate();
			return *t_singleton;
		}

		static T* get_singleton_pointer()
		{
			if (!is_initialized())
				instantizate();
			return t_singleton;
		}

		static bool is_initialized()
		{
			return (t_singleton != nullptr);
		}

	protected:
		Singleton()
		{
			t_singleton = static_cast<T*>(this);
		}
		~Singleton()
		{
			t_singleton = nullptr;
		}

	private:
		Singleton(Singleton const&) = delete;
		Singleton& operator=(Singleton const&) = delete;

		static void instantizate()
		{
			t_singleton = new T();
			assert(t_singleton != nullptr);
		}

		static T* t_singleton;
	};

	template<typename T>
	T* Singleton<T>::t_singleton = nullptr;

}

#endif
