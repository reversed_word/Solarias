#ifndef SOLARIAS_UTILS_THREADPOOL_H_
#define SOLARIAS_UTILS_THREADPOOL_H_
#pragma once

#include <thread>
#include <vector>
#include <deque>
#include <mutex>
#include <condition_variable>
#endif

namespace engine
{

	class ThreadPool {
	public:
		ThreadPool(size_t);
		~ThreadPool();

		template < class T >
		void enqueue(T thread);
	private:
		friend class WorkerThread;

		std::vector<std::thread> worker_threads_;
		std::deque<std::function<void()>> task_deque_;

		std::mutex queue_mutex_;
		std::condition_variable condition_;
		bool stop_b_;
	};

}