#ifndef SOLARIAS_UTILS_WORKERTHREAD_H_
#define SOLARIAS_UTILS_WORKERTHREAD_H_
#pragma once

#include "utils/thread-pool.h"
#endif

namespace engine
{

	class WorkerThread {
	public:
		WorkerThread(ThreadPool& pool);

		void operator()();
	protected:
		ThreadPool &pool_;
	};

}