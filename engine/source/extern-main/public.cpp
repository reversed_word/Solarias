#include <exception>
#include <iostream>
#include <memory>

#include "lib/precompiled-headers.h"

#include "engine/globals.h"
#include "utils/logger.h"
#include "extern-main/public.h"
#include "graphics/video-mode.h"
#include "graphics/renderer.h"

// Forward declarations and local fields go here!
// Functions/fields declared outside header files is pretty much the equivalent to "private" functions 

namespace engine
{

	bool g_sdl_initialized; // extern

	int elapsed_frames;
	double milliseconds_per_frame;
	bool quit_bool;
	bool restart_bool;
	bool restart_everything_bool;
	bool should_draw;
	bool shut_bool;
	RunStatus status;

	// std::vector<sf::Drawable*> drawbuffer;

	//void count_fps();
	void draw();
	void populate_events(const SDL_Event*);
	void populate_input();
	bool should_quit();
	void shutdown();
	void thread_tick();
	void update();

	// End of forward declarations

	/*void count_fps() {
		if (!fps_timer.is_running())
			fps_timer.start();
		elapsed_frames++;

		auto frames_per_second = kNanosecondsInSecond / fps_timer.get_elapsed_time(PERIOD_NANOSECONDS);
		milliseconds_per_frame = frames_per_second / kNanosecondsInMilliSecond;

		if (frames_per_second <= 1.0) {
			printf("%f frames per second\n%f ms/frame", frames_per_second, milliseconds_per_frame);
			elapsed_frames = 0;
			fps_timer.reset();
		}
	}*/

	void draw()
	{
		RENDERER.draw_frame();

		RENDERER.finalize_frame();

		SDL_GL_SwapWindow(VIDEOMODE.get_window());

		/*
		g_window->clear();
		try {
			for (std::vector<sf::Drawable*>::iterator i = drawbuffer.begin(); i != drawbuffer.end() ; ++i)
				g_window->draw(**i);
		}
		catch (std::exception error_exception)
		{
			std::cout << error_exception.what() << std::endl;
		}
		g_window->display();
		*/
	}

	void init_config()
	{

	}

	bool init_graphics()
	{
		if (SDL_INIT_EVERYTHING < 0)
		{
			LOGERROR(SDL_GetError());
			return false;
		}

		!VIDEOMODE.init_sdl();

		return true;
		/* g_window = new sf::RenderWindow(sf::VideoMode(400, 400), "Solarias");
		drawbuffer = std::vector<sf::Drawable*>();

		shape = sf::CircleShape(100.f);
		shape.setFillColor(sf::Color::Green);

		drawbuffer.push_back(&shape);

		shape1 = sf::CircleShape(20.f);
		shape1.setFillColor(sf::Color::Red);
		drawbuffer.push_back(&shape1);
		*/
	}

	void init_logging()
	{
		LOGGER.all_write("Successful warmup!");
	}

	void init_threadpool()
	{
		g_thread_pool = new ThreadPool(sizeof(size_t));
	}

	void populate_events(const SDL_Event* event)
	{
		switch (event->type)
		{
		case SDL_QUIT:
		{
			VIDEOMODE.shut_window();
		}
		};
	}

	void populate_input()
	{

	}

	void post_config()
	{

	}

	RunStatus run_or_die()
	{
		status = RUNNING;
		restart_bool = true; // equivalent to asking if we are running this for the first time
		while (restart_bool && !restart_everything_bool) // while we want to restart, but not everything
		{
			quit_bool = false;
			restart_bool = false;
			restart_everything_bool = false;
			SDL_Event event;

			g_sdl_initialized = init_graphics();
			post_config();
			while (!should_quit())
			{
				try
				{
					//count_fps();
					while (SDL_PollEvent(&event))
						populate_events(&event);
					if (!should_quit())
					{
						update();
						if (!should_quit())
							draw();
					}
				}
				catch (std::exception exception)
				{
					std::cout
						<< "Non-fatal Error!\n"
						<< exception.what() << std::endl;
				}
			}
			shutdown();
		}
		if (restart_everything_bool)
			return (status = RESTART);

		return (status = SUCCESS);
	}

	bool should_quit()
	{
		return restart_bool
			|| quit_bool
			|| !VIDEOMODE.is_window_open();
	}

	void shutdown()
	{
		SDL_Quit();
	}

	void thread_tick()
	{

	}

	void update()
	{
	}

}