#include "graphics\camera.h"

namespace engine
{

	Camera::Camera()
	{
		viewport_.x_pos_ = 0;
		viewport_.y_pos_ = 0;
		viewport_.height_ = VIDEOMODE.get_height();
		viewport_.width_ = VIDEOMODE.get_width();
	}

	void Camera::set_projection(const float& near_viewplane_x, const float& far_viewplane_x, const float& fov_x)
	{
		near_viewplane_ = near_viewplane_x;
		far_viewplane_ = far_viewplane_x;
		fov_ = fov_x;

		float factor = 1.0f / tanf(fov_ / 2);

		float view_window_width = factor / viewport_.get_aspect();
		float view_window_height = factor;

		float a_x = -((far_viewplane_ + near_viewplane_) / (far_viewplane_ - near_viewplane_));
		float b_x = -((2 * far_viewplane_ * near_viewplane_) / (far_viewplane_ - near_viewplane_));

		float_matrix_ = { {
			{	view_window_width,				0.0f,				0.0f,				0.0f }, // Row One
			{				0.0f,	view_window_height,				0.0f,				0.0f }, // Row Two
			{				0.0f,				0.0f,				a_x,				b_x	 }, // Row Three
			{				0.0f,				0.0f,				-1.0f,				0.0f }  // Row Four
		} };
	}

}