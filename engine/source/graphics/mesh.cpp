#include <assert.h>

#include "graphics\mesh.h"

namespace engine {

	Mesh::Mesh(std::vector<Vertex> vertices,
		std::vector<VertexIndex> vertex_indices,
		ObjectRenderingMode rendering_mode)

		: indices_number_(vertices.size()), rendering_mode_(rendering_mode),
		vertices_(vertices), vertex_indices_(vertex_indices)
	{
		switch (rendering_mode_)
		{
		case OBJECT_RENDER_MODE_USE_VBO:
		{
			create(true);
			update_buffers();
			break;
		}
		case OBJECT_RENDER_MODE_USE_ARRAYS:
			create(false);
			break;
		}
	}

	void Mesh::reshape(std::vector<Vertex> new_vertices, std::vector<VertexIndex> new_vertex_indices)
	{
		vertices_ = new_vertices;
		vertex_indices_ = new_vertex_indices;
	}

	void Mesh::create(bool use_buffer)
	{
		if (use_buffer) {
			interleaved_buffer_.create();
			index_buffer_.create();
		}
	}

	void Mesh::update()
	{
		assert(interleaved_buffer_.created());
		assert(index_buffer_.created());
		indices_number_ = vertex_indices_.size();
	}

	void Mesh::clear()
	{
	}

	void Mesh::render() 
	{
		if (is_buffered() && rendering_mode_ == OBJECT_RENDER_MODE_USE_VBO)
			render_from_buffers();
		else
			render_from_arrays();
	}

	MaterialIndex Mesh::get_material()
	{
		return material_index_;
	}

	void Mesh::set_material(MaterialIndex material_index)
	{
		material_index_ = material_index;
	}

	ObjectRenderingMode Mesh::get_render_mode()
	{
		return rendering_mode_;
	}

	void Mesh::set_render_mode(ObjectRenderingMode mode)
	{
		rendering_mode_ = mode;
		if (rendering_mode_ == OBJECT_RENDER_MODE_USE_VBO)
		{
			if (!is_buffered())
				create(true);
			update_buffers();
		}
	}

	bool Mesh::is_buffered()
	{
		return interleaved_buffer_.created() 
			&& index_buffer_.created();
	}

	void Mesh::update_buffers()
	{
		assert(interleaved_buffer_.created());
		assert(index_buffer_.created());

		indices_number_ = vertex_indices_.size();

		interleaved_buffer_.bind(GL_ARRAY_BUFFER);

		glBufferData(GL_ARRAY_BUFFER,
			sizeof(Vertex) * vertices_.size(), &vertices_[0], GL_STATIC_DRAW);

		index_buffer_.bind(GL_ELEMENT_ARRAY_BUFFER);

		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			sizeof(VertexIndex) * vertex_indices_.size(), &vertex_indices_[0], GL_STATIC_DRAW);
	}

	void Mesh::render_from_buffers()
	{
		interleaved_buffer_.bind(GL_ARRAY_BUFFER);

		glVertexAttribPointer(VERTEX_ATTRIBUTE_POSITION, 3,
			GL_FLOAT, false, sizeof(Vertex), nullptr);
		glVertexAttribPointer(VERTEX_ATTRIBUTE_NORMALS, 3,
			GL_FLOAT, false, sizeof(Vertex),
			reinterpret_cast<const void*>(sizeof(Vector3<float>)));
		glVertexAttribPointer(VERTEX_ATTRIBUTE_TEXCOORDINATES, 2,
			GL_FLOAT, false, sizeof(Vertex),
			reinterpret_cast<const void*>(sizeof(Vector3<float>) + sizeof(Vector3<float>)));

		index_buffer_.bind(GL_ELEMENT_ARRAY_BUFFER);

		glDrawElements(GL_TRIANGLES, indices_number_, GL_UNSIGNED_INT, nullptr);
	}

	void Mesh::render_from_arrays()
	{
		VertexBufferObject::unbind_all();

		glVertexAttribPointer(VERTEX_ATTRIBUTE_POSITION, 3,
			GL_FLOAT, false, sizeof(Vertex), &vertices_[0].position);
		glVertexAttribPointer(VERTEX_ATTRIBUTE_NORMALS, 3,
			GL_FLOAT, false, sizeof(Vertex), &vertices_[0].normals);
		glVertexAttribPointer(VERTEX_ATTRIBUTE_TEXCOORDINATES, 2,
			GL_FLOAT, false, sizeof(Vertex), &vertices_[0].texture_coordinates);

		glDrawElements(GL_TRIANGLES, vertex_indices_.size(), GL_UNSIGNED_INT, &vertex_indices_[0]);
	}

}; // END namespace engine