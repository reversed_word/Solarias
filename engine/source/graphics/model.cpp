#include <string>

#include "utils\logger.h"
#include "graphics\model.h"

// Waveform OBJ header tokens
#define MODEL_TOKEN_OBJECT_NAME_WAVEFORMOBJ "o"
#define MODEL_TOKEN_VERTEX_POSITION_WAVEFORMOBJ "v"
#define MODEL_TOKEN_VERTEX_TEXTURE_COORDINATES_WAVEFORMOBJ "vt"
#define MODEL_TOKEN_VERTEX_NORMALS_WAVEFORMOBJ "vn"
#define MODEL_TOKEN_FACE_WAVEFORMOBJ "f"
#define MODEL_TOKEN_NEW_MESH_WAVEFORMOBJ "usemtl"

namespace engine
{
	void Model::render()
	{
		auto iterator = shared_model_info_->meshes.begin();
		while (iterator != shared_model_info_->meshes.end())
		{
			// this line below is equivalent to: iterator->get()->render();
			(*iterator)->render();

			++iterator;
		}
	}

	bool Model::load_from(const char* model_file_path)
	{
		std::string path_str = model_file_path;
		const char* file_ext = path_str.substr(path_str.find_last_of('.') + 1).c_str();

		ModelFile model_file;

		if (file_ext == "obj")
			model_file.file_format = WAVEFORMOBJ;

		model_file.file_path = model_file_path;
		return load_from(model_file);
	}

	bool Model::load_from(const ModelFile& model_file)
	{
		switch (model_file.file_format)
		{
		case WAVEFORMOBJ:
			return load_from_obj(model_file.file_path);
		default:
			LOGERROR("[MODELLOADER] > [%s] < uses a format unsupported by this engine!", model_file.file_path);
		}
	}

	// TODO: implement materials

	bool Model::load_from_obj(const char* model_file_path)
	{
		FILE* obj_file = fopen(model_file_path, "r");

		if (obj_file == nullptr)
		{
			LOGERROR("It is impossible to open the file!");
			return false;
		}

		std::vector<Vector3<float>> vertex_positions;
		std::vector<Vector3<float>> vertex_normals;
		std::vector<Vector2<float>> vertex_texture_coordinates;

		std::vector<Vertex> vertices;
		std::vector<VertexIndex> vertex_indices;

		const char* header_string;
		int res, read_lines;
		res = read_lines = 0; // assigns both res and read_lines to zero

		// =============================================================
		// ORDER OF MESH ELEMENTS IN WAVEFORM OBJ
		//
		// 1. "mtllib"
		// 2. Object (name)
		// 3. Vertices
		// 4. UV Coordinates
		// 5. Normals
		// 6. "usemtl" - new mesh
		// 7. Faces
		//

		while ((res = fscanf(obj_file, "%s", header_string)) != EOF)
		{
			if (strcmp(header_string, MODEL_TOKEN_OBJECT_NAME_WAVEFORMOBJ) == 0)
			{
				if (!shared_model_info_->meshes.empty()) // checks if we are about to create the first mesh
				{
					shared_model_info_->meshes.back()->reshape(vertices, vertex_indices);
					
					vertex_positions.clear();
					vertex_normals.clear();
					vertex_texture_coordinates.clear();

					vertices.clear();
					vertex_indices.clear();
				}

				shared_model_info_->meshes.push_back(std::shared_ptr<Mesh>());
				shared_model_info_->meshes.back().reset(new Mesh());
			}
			else if (strcmp(header_string, MODEL_TOKEN_VERTEX_POSITION_WAVEFORMOBJ) == 0)
			{
				Vector3<float> new_vertex_position;
				fscanf(obj_file, "%f %f %f\n",
					&new_vertex_position.x,
					&new_vertex_position.y,
					&new_vertex_position.z);
				vertex_positions.push_back(new_vertex_position);
			}
			else if (strcmp(header_string, MODEL_TOKEN_VERTEX_TEXTURE_COORDINATES_WAVEFORMOBJ) == 0)
			{
				Vector2<float> new_vertex_texture_coordinates;
				fscanf(obj_file, "%f %f\n",
					&new_vertex_texture_coordinates.x,
					&new_vertex_texture_coordinates.y);
				vertex_texture_coordinates.push_back(new_vertex_texture_coordinates);
			}
			else if (strcmp(header_string, MODEL_TOKEN_VERTEX_NORMALS_WAVEFORMOBJ) == 0)
			{
				Vector3<float> new_vertex_normals;
				fscanf(obj_file, "%f %f %f\n",
					&new_vertex_normals.x,
					&new_vertex_normals.y,
					&new_vertex_normals.z);
				vertex_normals.push_back(new_vertex_normals);
			}
			else if (strcmp(header_string, MODEL_TOKEN_NEW_MESH_WAVEFORMOBJ) == 0)
			{

			}
			else if (strcmp(header_string, MODEL_TOKEN_FACE_WAVEFORMOBJ) == 0)
			{
				// Faces in obj are composed of three sets of unique indices for position, normal and UV
				// like this:
				//		vertex/UV/noraml vertex/UV/normal vertex/UV/normal
				// MeshIndexSet contains unique indices to position, normal and UV for each respective vertex
				std::vector<MeshIndexSet> index_sets;
				index_sets.assign(3, MeshIndexSet());

				size_t matches = fscanf(obj_file, "%d/%d/%d %d/%d/%d %d/%d/%d\n",
					&index_sets[0].position_index,
					&index_sets[0].texture_coordinates_index,
					&index_sets[0].normals_index,
					&index_sets[1].position_index,
					&index_sets[1].texture_coordinates_index,
					&index_sets[1].normals_index,
					&index_sets[2].position_index,
					&index_sets[2].texture_coordinates_index,
					&index_sets[2].normals_index);

				if (matches != 9)
				{
					if (matches > 9)
						LOGERROR("[MODELLOADER] > [%s] < amount of faces exceed excepted amount, >9", model_file_path);
					else
						LOGERROR("[MODELLOADER] > [%s] < amount of faces fall behind excepted amount, <9", model_file_path);
					return false;
				}

				for (int index = 0; index < index_sets.size(); index++)
				{
					auto iterator = std::find(
						vertices.begin(),
						vertices.end(), 
						index_sets[index].to_vertex(
							nullptr,
							&vertex_positions,
							&vertex_texture_coordinates,
							&vertex_normals
							)
						);

					if (iterator == vertices.end()) // not found
					{
						// OBJ (Waveform) starts indexing from 1, convert to C++'s 0
						vertices.push_back(Vertex(nullptr,
							vertex_positions[index_sets[index].position_index - 1],
							vertex_normals[index_sets[index].normals_index - 1],
							vertex_texture_coordinates[index_sets[index].texture_coordinates_index - 1]));

						vertex_indices.push_back(vertices.size() - 1); // save index
					}
					else // found
					{
						size_t index = iterator - vertices.begin();

						vertex_indices.push_back(index);
					}
				}
			}

			read_lines++;

		} // end while loop

		if (read_lines < 1)
			LOGERROR("[MODELLOADER] > [%s] < model file is empty!", model_file_path);
		else // skip this if the read_lines is 0, avoiding errors
		{
			shared_model_info_->meshes.back()->reshape(vertices, vertex_indices);
		}

		return true;
	}

}; // END namespace engine