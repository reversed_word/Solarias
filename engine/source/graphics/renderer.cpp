#include <iostream>

#include "lib/precompiled-headers.h"

#include "lib/opengl.h"

#include "graphics\model.h"
#include "graphics\renderer.h"
#include "graphics\video-mode.h"

#define RANDCOLOR glColor3f((rand() % 65 + 15)/50, (rand() % 65 + 15)/50, (rand() % 65 + 15)/50)

namespace engine
{

	Model* debug_model;

	float rquad = 0;

	/*void draw_quad()
	{
		glTranslatef(0.0f, 0.0f, -12.0f);

		glRotatef(rquad, 1.0f, 1.0f, 1.0f);
		glBegin(GL_TRIANGLES);
		glVertex3f(1.0f, 1.0f, 1.0f);
		RANDCOLOR;
		glVertex3f(1.0f, -1.0f, 1.0f);
		RANDCOLOR;
		glVertex3f(-1.0f, -1.0f, 1.0f);
		RANDCOLOR;

		glVertex3f(-1.0f, -1.0f, 1.0f);
		RANDCOLOR;
		glVertex3f(-1.0f, 1.0f, 1.0f);
		RANDCOLOR;
		glVertex3f(1.0f, 1.0f, 1.0f);
		RANDCOLOR;

		//2nd face
		glVertex3f(1.0f, 1.0f, 1.0f);
		RANDCOLOR;
		glVertex3f(1.0f, 1.0f, -1.0f);
		RANDCOLOR;
		glVertex3f(1.0f, -1.0f, -1.0f);
		RANDCOLOR;

		glVertex3f(1.0f, -1.0f, -1.0f);
		RANDCOLOR;
		glVertex3f(1.0f, 1.0f, 1.0f);
		RANDCOLOR;
		glVertex3f(1.0f, -1.0f, 1.0f);
		RANDCOLOR;

		//3rd face
		glVertex3f(1.0f, 1.0f, -1.0f);
		RANDCOLOR;
		glVertex3f(1.0f, -1.0f, -1.0f);
		RANDCOLOR;
		glVertex3f(-1.0f, -1.0f, -1.0f);
		RANDCOLOR;

		glVertex3f(-1.0f, -1.0f, -1.0f);
		RANDCOLOR;
		glVertex3f(-1.0f, 1.0f, -1.0f);
		RANDCOLOR;
		glVertex3f(1.0f, 1.0f, -1.0f);
		RANDCOLOR;

		//4th face
		glVertex3f(-1.0f, 1.0f, 1.0f);
		RANDCOLOR;
		glVertex3f(-1.0f, 1.0f, -1.0f);
		RANDCOLOR;
		glVertex3f(-1.0f, -1.0f, -1.0f);
		RANDCOLOR;

		glVertex3f(-1.0f, -1.0f, -1.0f);
		RANDCOLOR;
		glVertex3f(-1.0f, 1.0f, 1.0f);
		RANDCOLOR;
		glVertex3f(-1.0f, -1.0f, 1.0f);
		RANDCOLOR;

		//5th face
		glVertex3f(1.0f, 1.0f, 1.0f);
		RANDCOLOR;
		glVertex3f(-1.0f, 1.0f, 1.0f);
		RANDCOLOR;
		glVertex3f(-1.0f, 1.0f, -1.0f);
		RANDCOLOR;

		glVertex3f(-1.0f, 1.0f, -1.0f);
		RANDCOLOR;
		glVertex3f(1.0f, 1.0f, -1.0f);
		RANDCOLOR;
		glVertex3f(1.0f, 1.0f, 1.0f);
		RANDCOLOR;

		//6th
		glVertex3f(1.0f, -1.0f, 1.0f);
		RANDCOLOR;
		glVertex3f(-1.0f, -1.0f, 1.0f);
		RANDCOLOR;
		glVertex3f(-1.0f, -1.0f, -1.0f);
		RANDCOLOR;

		glVertex3f(-1.0f, -1.0f, -1.0f);
		RANDCOLOR;
		glVertex3f(1.0f, -1.0f, -1.0f);
		RANDCOLOR;
		glVertex3f(1.0f, -1.0f, 1.0f);
		RANDCOLOR;

		glEnd();

		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, 0.0f);

		rquad += 0.05f;
	}*/

	Renderer::Renderer()
	{
		setup_opengl();
	}

	Renderer::~Renderer()
	{
	}

	void Renderer::setup_opengl()
	{
		opengl_setup_buffers();

		glShadeModel(GL_SMOOTH);

		glCullFace(GL_BACK);
		glFrontFace(GL_CCW);
		glEnable(GL_FRONT_FACE);

		glClearColor(0, 0, 0, 0);

		glViewport(0, 0, VIDEOMODE.get_width(), VIDEOMODE.get_height());

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glFrustum(-0.1333, 0.1333, -0.1, 0.1, 0.5, 25.0);
		//glOrtho(-0.2333, 0.2333, -0.2, 0.2, 1.0, 25.0);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		debug_model = new Model();
		debug_model->load_from_obj("/model/suzanne.obj");
	}

	void Renderer::prepare_frame_buffer()
	{
		framebuffer_ = 0;
	}

	void Renderer::begin_frame()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // should be always first
		glLoadIdentity();
	}

	void Renderer::draw_frame()
	{
		begin_frame();

		debug_model->render();

		//draw_quad();

		finish_frame();
	}

	void Renderer::finish_frame()
	{
	}

	void Renderer::finalize_frame()
	{
		glFlush();
	}

}