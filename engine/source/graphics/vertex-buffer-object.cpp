#include <assert.h>

#include "graphics\vertex-buffer-object.h"

namespace engine {

	unsigned VertexBufferObject::current_array_buffer_id_ = 0;
	unsigned VertexBufferObject::current_element_array_buffer_id_ = 0;

	void VertexBufferObject::create() {
		assert(buffer_id_ == 0);

		glGenBuffers(1, &buffer_id_);
	}

	void VertexBufferObject::destroy() {
		assert(buffer_id_ != 0);

		switch (usage_flag_) {
		case GL_ARRAY_BUFFER:
			if (current_array_buffer_id_ == buffer_id_) {
				current_array_buffer_id_ = 0;
				glBindBuffer(usage_flag_, 0);
			}
			break;
		case GL_ELEMENT_ARRAY_BUFFER:
			if (current_element_array_buffer_id_ == buffer_id_) {
				current_element_array_buffer_id_ = 0;
				glBindBuffer(usage_flag_, 0);
			}
			break;
		}

		glDeleteBuffers(1, &buffer_id_);
		buffer_id_ = 0;
	}

	void VertexBufferObject::bind(const unsigned flag) {
		usage_flag_ = flag;

		glBindBuffer(usage_flag_, buffer_id_);
	}

	void VertexBufferObject::force_bind(const unsigned flag) {
		usage_flag_ = flag;

		switch (usage_flag_) {
		case GL_ARRAY_BUFFER:
			current_array_buffer_id_ = buffer_id_;
			break;
		case GL_ELEMENT_ARRAY_BUFFER:
			current_element_array_buffer_id_ = buffer_id_;
			break;
		}

		glBindBuffer(usage_flag_, buffer_id_);
	}

	void VertexBufferObject::unbind()
	{
		unbind(usage_flag_);
	}

	void VertexBufferObject::unbind(const unsigned flag)
	{
		glBindBuffer(flag, 0);

		switch (flag) {
		case GL_ARRAY_BUFFER:
			current_array_buffer_id_ = 0;
			break;
		case GL_ELEMENT_ARRAY_BUFFER:
			current_element_array_buffer_id_ = 0;
			break;
		}
	}

	void VertexBufferObject::unbind_all()
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		current_array_buffer_id_ = 0;
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		current_element_array_buffer_id_ = 0;
	}

}; // END namespace engine