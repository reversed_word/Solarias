#include <iostream>

#include "graphics/video-mode.h"
#include "utils/Logger.h"

#define SOLARIAS_PROJECT_NAME "Solarias"
#define DEFAULT_WINDOW_WIDTH 640
#define DEFAULT_WINDOW_HEIGHT 480

namespace engine
{

	VideoMode::VideoMode()
		: width_(DEFAULT_WINDOW_WIDTH), height_(DEFAULT_WINDOW_HEIGHT), fullscreen_bool_(false),
		sdl_window_(nullptr), gl_context_(nullptr)
	{

	}

	VideoMode::~VideoMode()
	{
	}

	bool VideoMode::init_sdl()
	{
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
		SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

		set_video_mode_or_die();

		return true;
	}

	bool VideoMode::update_resolution(int width_i, int height_i)
	{
		// TODO: fix dis
		if (sdl_window_)
			SDL_SetWindowSize(sdl_window_, width_i, height_i);
		return true;
	}

	void VideoMode::update_position(int x_i, int y_i)
	{
		if (sdl_window_)
			SDL_SetWindowPosition(sdl_window_, x_i, y_i);
	}

	void VideoMode::set_window_caption(const char* new_caption_string)
	{
		if (sdl_window_)
			SDL_SetWindowTitle(sdl_window_, new_caption_string);
	}

	const char* VideoMode::get_window_caption() const
	{
		if (sdl_window_)
			return SDL_GetWindowTitle(sdl_window_);
		return nullptr;
	}

	void VideoMode::set_fullscreen_enabled(bool state_b)
	{
		fullscreen_bool_ = state_b;
	}

	bool VideoMode::is_fullscreen_enabled() const
	{
		return fullscreen_bool_;
	}

	bool VideoMode::toggle_fullscreen()
	{
		return (fullscreen_bool_ = !fullscreen_bool_);
	}

	bool VideoMode::is_window_open() const
	{
		return is_open_bool_;
	}

	bool VideoMode::open_window()
	{
		if (!sdl_window_)
		{
			if ((sdl_window_ = SDL_CreateWindow(
				SOLARIAS_PROJECT_NAME,
				SDL_WINDOWPOS_CENTERED,
				SDL_WINDOWPOS_CENTERED,
				width_,
				height_,
				SDL_WINDOW_OPENGL)) == nullptr ||
				(gl_context_ = SDL_GL_CreateContext(sdl_window_)) == nullptr)
			{
				LOGERROR(SDL_GetError());
				return false;
			}
			is_open_bool_ = true;
			return true;
		}
		return false;
	}

	bool VideoMode::shut_window()
	{
		if (sdl_window_)
		{
			SDL_DestroyWindow(sdl_window_);
			SDL_GL_DeleteContext(gl_context_);
			is_open_bool_ = false;
			return true;
		}
		return false;
	}

	void VideoMode::shutdown()
	{

	}

	unsigned VideoMode::get_width() const
	{
		return width_;
	}

	unsigned VideoMode::get_height() const
	{
		return height_;
	}

	float VideoMode::get_ratio() const
	{
		return float(width_) / float(height_);
	}

	unsigned VideoMode::get_bpp() const
	{
		return 0;
	}

	unsigned VideoMode::get_best_bpp() const
	{
		return 0;
	}

	SDL_Window* VideoMode::get_window() const
	{
		return sdl_window_;
	}

	bool VideoMode::set_video_mode_or_die()
	{
		try
		{
			if ((sdl_window_ = SDL_CreateWindow(
				SOLARIAS_PROJECT_NAME,
				SDL_WINDOWPOS_CENTERED,
				SDL_WINDOWPOS_CENTERED,
				width_,
				height_,
				SDL_WINDOW_OPENGL)) == nullptr ||
				(gl_context_ = SDL_GL_CreateContext(sdl_window_)) == nullptr)
			{
				LOGERROR(SDL_GetError());
			}

		}
		catch (std::exception error)
		{
			LOGERROR(error.what());
			return false;
		}
		return true;
	}

}