#include "lib\opengl.h"

void engine::opengl_setup_buffers()
{
	glGenVertexArrays(1, VERTEX_ARRAY_OBJECT);
	glBindVertexArray(VERTEX_ARRAY_OBJECT);

	glEnableVertexAttribArray(VERTEX_ATTRIBUTE_POSITION);
	glEnableVertexAttribArray(VERTEX_ATTRIBUTE_NORMALS);
	glEnableVertexAttribArray(VERTEX_ATTRIBUTE_TEXCOORDINATES);
}
