#include <sys/stat.h>
#include <fstream>
#include <iostream>
#include <stdarg.h>

#include "utils/channel-type.h"
#include "utils/log-channel.h"
#include "utils\logger.h"

namespace engine
{

	LogChannel::LogChannel(const char* name_str)
	{
		chan_name_str_ = name_str;
		channel_type_ = NORMAL_LOG;

		ensure();
	}

	LogChannel::LogChannel(const char* name_str, ChannelType channel_type)
	{
		chan_name_str_ = name_str;
		channel_type_ = channel_type;

		ensure();
	}

	LogChannel::~LogChannel()
	{
	}

	void LogChannel::close()
	{
	}

	void LogChannel::ensure()
	{
		std::fstream file_fstream_ = ensure_file();
		if (!file_exists() && !file_fstream_.is_open())
		{
			std::cout << "[" << chan_name_str_.c_str() << "] >> " << get_log_file_path_string().c_str() << " doesn't exist!" << std::endl;
			try
			{
				file_fstream_.open(get_log_file_path_string());
				file_fstream_.close();
			}
			catch (std::exception error_exception)
			{
				file_fstream_ = std::fstream(get_log_file_path_string());
				write(error_exception.what());
			}
		}
	}

	bool LogChannel::file_exists()
	{
		struct stat buffer_stat;
		return (stat(chan_name_str_.c_str(), &buffer_stat) == 0);
	}

	FILE* LogChannel::open()
	{
		return fopen(get_log_file_path_string().c_str(), "w");
	}

	std::fstream LogChannel::ensure_file()
	{
		return std::fstream(get_log_file_path_string());
	}

	void LogChannel::write(std::string const message_string)
	{
		write(message_string.c_str());
	}

	void LogChannel::write(const char* const message_format, ...)
	{
		va_list args;
		ensure();

		FILE* log_file = open();

		va_start(args, message_format);
		vfprintf(log_file, message_format, args);
		va_end(args);

		fclose(log_file);

		printf("[%s] >> ", chan_name_str_.c_str());

		va_start(args, message_format);
		vprintf(message_format, args);
		va_end(args);

		printf("\n");
	}

}