#include "utils\logger.h"

namespace engine
{

	std::map<const char*, LogChannel*> Logger::backup_map_ = std::map<const char*, LogChannel*>();

	void Logger::setup()
	{
		add_channel("master");
		add_channel("error", ERROR_LOG);
	}

	void Logger::add_channel(const char* channel_name_str)
	{
		LogChannel* new_channel = new LogChannel(channel_name_str);
		std::cout << "Creating channel: " << channel_name_str << "..." << std::endl;
		log_channel_map_.insert(std::pair<const char*, LogChannel*>(channel_name_str, new_channel));
	}

	void Logger::add_channel(const char* channel_name_str, ChannelType channel_type)
	{
		LogChannel* new_channel = new LogChannel(channel_name_str, channel_type);
		std::cout << "Creating channel: " << channel_name_str << "..." << std::endl;
		log_channel_map_.insert(std::pair<const char*, LogChannel*>(channel_name_str, new_channel));
	}

	void Logger::all_write(const char* message_text)
	{
		for (auto iterator = log_channel_map_.begin(); iterator != log_channel_map_.end(); ++iterator)
			iterator->second->write(message_text);
	}

	bool Logger::channel_exists(const char* channel_name_string)
	{
		for (auto iterator = log_channel_map_.begin(); iterator != log_channel_map_.end(); ++iterator)
		{
			if (iterator->first == channel_name_string)
				return true;
		}
		return false;
	}

	std::list<const char*> Logger::get_channel_names()
	{
		std::list<const char*> return_list = std::list<const char*>();

		for (auto iterator = log_channel_map_.begin(); iterator != log_channel_map_.end(); ++iterator)
		{
			return_list.push_back(iterator->first);
		}

		return return_list;
	}

	std::list<const char*> Logger::get_channel_names_by_type(ChannelType channel_type)
	{
		std::list<const char*> return_list = std::list<const char*>();

		for (auto iterator = log_channel_map_.begin(); iterator != log_channel_map_.end(); ++iterator)
		{
			if (iterator->second->channel_type_ == channel_type)
			{
				return_list.push_back(iterator->first);
			}
		}

		return return_list;
	}

	LogChannel* Logger::operator[](const char* channel_name_str) const
	{
		try
		{
			if (log_channel_map_.find(channel_name_str)->second == nullptr)
				throw;
			return log_channel_map_.find(channel_name_str)->second;
		}
		catch (std::exception error_exception)
		{
			std::cout << error_exception.what() << std::endl;
			return nullptr;
		}
	}

	Logger::Logger()
	{
		if (backup_is_not_empty())
		{
			std::cout << backup_is_not_empty() << std::endl;
			log_channel_map_ = backup_map_;
		}
		else
		{
			setup();
		}
	}

	Logger::~Logger()
	{
		backup_map_ = log_channel_map_;
	}

	bool Logger::backup_is_not_empty()
	{
		return !backup_map_.empty();
	}

}