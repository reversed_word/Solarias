#include "utils/thread-pool.h"
#include "utils/worker-thread.h"

namespace engine
{

	ThreadPool::ThreadPool(size_t threads_t) : stop_b_(false)
	{
		for (size_t i = 0; i < threads_t; i++)
			worker_threads_.push_back(std::thread(WorkerThread(*this)));
	}

	ThreadPool::~ThreadPool()
	{
		stop_b_ = true;
		condition_.notify_all();

		for (size_t i = 0; i < worker_threads_.size(); i++)
			worker_threads_[i].join();
	}

	template <class T>
	void ThreadPool::enqueue(T func)
	{
		{
			std::unique_lock<std::mutex>
				lock(queue_mutex_);
			task_deque_.push_back(std::function<void()>(func));
		}
		condition_.notify_one();
	}

}