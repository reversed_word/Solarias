#include "utils/worker-thread.h"

namespace engine
{

	WorkerThread::WorkerThread(ThreadPool& pool) : pool_(pool)
	{
	}

	void WorkerThread::operator()()
	{
		std::function<void()> task_function;
		while (true)
		{
			{
				std::unique_lock<std::mutex>
					lock(pool_.queue_mutex_);
				while (!pool_.stop_b_ && pool_.task_deque_.empty())
				{
					pool_.condition_.wait(lock);
				}
				if (pool_.stop_b_)
					return;
				task_function = pool_.task_deque_.front();
				pool_.task_deque_.pop_front();
			}
			task_function();
		}
	}

}