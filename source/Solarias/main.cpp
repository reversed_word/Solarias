#include <iostream>

// We need these preprocessed...
// ReSharper disable CppUnusedIncludeDirective
#include "lib/precompiled-headers.h"

#include "lib/opengl.h"
// ReSharper restore CppUnusedIncludeDirective

#include "extern-main/public.h"

void parseArgurments(char *args) 
{
	
}

void preInit()
{
	
}

extern "C"
int main(int argc, char *argv[])
{
	engine::init_logging();
	engine::RunStatus game_run_statusstatus = engine::run_or_die();

	std::cout << "End of thread!\n"
		<< "The run_or_die() returned " << game_run_statusstatus
		<< std::endl;
	std::cin.get();

	return static_cast<int>(game_run_statusstatus);
}